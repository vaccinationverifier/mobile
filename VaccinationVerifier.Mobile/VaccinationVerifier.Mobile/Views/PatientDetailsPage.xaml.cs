﻿using System;
using VaccinationVerifier.Mobile.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace VaccinationVerifier.Mobile.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PatientDetailsPage : ContentPage
    {
        public PatientDetailsPage()
        {
            InitializeComponent();
            BindingContext = new PatientDetailsViewModel();
        }

        private async void Vaccinations_Clicked(object sender, EventArgs e)
        {
            if (BindingContext is PatientDetailsViewModel patientDetails)
            {
                await Shell.Current.GoToAsync(
                    $"{nameof(PatientVaccinationsPage)}?{nameof(PatientVaccinationsViewModel.PatientId)}={patientDetails.Id}");
            }
        }
    }
}
