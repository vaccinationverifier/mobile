﻿using System;
using VaccinationVerifier.Mobile.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace VaccinationVerifier.Mobile.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginPage : ContentPage
    {
        public LoginPage()
        {
            InitializeComponent();
            this.BindingContext = new LoginViewModel();

        }

        private async void Login_Click(object sender, EventArgs args)
        {
            var login = LoginEntry.Text;
            var password = PasswordEntry.Text;

            var viewModel = this.BindingContext as LoginViewModel;
            await viewModel.LoginAsync(login, password);
        }
    }
}