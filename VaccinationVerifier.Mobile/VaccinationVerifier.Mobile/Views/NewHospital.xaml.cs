﻿using VaccinationVerifier.Mobile.ViewModels;
using Xamarin.Forms.Xaml;

namespace VaccinationVerifier.Mobile.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NewHospital
    {
        public NewHospital()
        {
            InitializeComponent();
            BindingContext = new NewHospitalViewModel();
        }
    }
}