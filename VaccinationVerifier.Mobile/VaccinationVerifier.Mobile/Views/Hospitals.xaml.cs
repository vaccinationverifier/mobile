﻿using VaccinationVerifier.Mobile.ViewModels;
using Xamarin.Forms.Xaml;

namespace VaccinationVerifier.Mobile.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Hospitals
    {
        private readonly HospitalsViewModel _viewModel;

        public Hospitals()
        {
            InitializeComponent();

            BindingContext = _viewModel = new HospitalsViewModel();
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
            _viewModel.OnAppearing();
        }
    }
}