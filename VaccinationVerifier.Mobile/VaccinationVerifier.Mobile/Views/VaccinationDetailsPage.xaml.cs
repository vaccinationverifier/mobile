﻿using VaccinationVerifier.Mobile.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace VaccinationVerifier.Mobile.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class VaccinationDetailsPage : ContentPage
    {
        public VaccinationDetailsPage()
        {
            InitializeComponent();

            BindingContext = new VaccinationDetailsViewModel();
        }

        private async void Verify_Clicked(object sender, System.EventArgs e)
        {
            var details = BindingContext as VaccinationDetailsViewModel;

            await details.VerifyAsync();
        }
    }
}