﻿using System.ComponentModel;
using VaccinationVerifier.Mobile.ViewModels;
using Xamarin.Forms;

namespace VaccinationVerifier.Mobile.Views
{
    public partial class ItemDetailPage : ContentPage
    {
        public ItemDetailPage()
        {
            InitializeComponent();
            BindingContext = new ItemDetailViewModel();
        }
    }
}