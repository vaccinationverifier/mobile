﻿using VaccinationVerifier.Mobile.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace VaccinationVerifier.Mobile.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Patients : ContentPage
    {
        private readonly PatientsViewModel _viewModel;

        public Patients()
        {
            InitializeComponent();

            BindingContext = _viewModel = new PatientsViewModel();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            _viewModel.OnAppearing();
        }
    }
}
