﻿using VaccinationVerifier.Mobile.ViewModels;
using Xamarin.Forms.Xaml;

namespace VaccinationVerifier.Mobile.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NewVaccine
    {
        public NewVaccine()
        {
            InitializeComponent();

            BindingContext = new NewVaccineViewModel();
        }
    }
}