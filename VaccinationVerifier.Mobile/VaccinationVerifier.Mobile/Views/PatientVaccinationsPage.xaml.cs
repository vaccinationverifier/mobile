﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VaccinationVerifier.Mobile.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace VaccinationVerifier.Mobile.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PatientVaccinationsPage : ContentPage
    {
        private readonly PatientVaccinationsViewModel _viewModel;

        public PatientVaccinationsPage()
        {
            InitializeComponent();

            BindingContext = _viewModel = new PatientVaccinationsViewModel();
        }
        
        protected override void OnAppearing()
        {
            base.OnAppearing();
            _viewModel.OnAppearing();
        }
    }
}