﻿using VaccinationVerifier.Mobile.ViewModels;
using Xamarin.Forms.Xaml;

namespace VaccinationVerifier.Mobile.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Vaccines
    {
        private readonly VaccinesViewModel _viewModel;

        public Vaccines()
        {
            InitializeComponent();

            BindingContext = _viewModel = new VaccinesViewModel();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            _viewModel.OnAppearing();
        }
    }
}