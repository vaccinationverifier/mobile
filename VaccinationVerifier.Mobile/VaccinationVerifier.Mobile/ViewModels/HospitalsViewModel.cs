﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;
using VaccinationVerifier.Mobile.Clients.Interfaces;
using VaccinationVerifier.Mobile.Models;
using VaccinationVerifier.Mobile.Views;
using Xamarin.Forms;

namespace VaccinationVerifier.Mobile.ViewModels
{
    public class HospitalsViewModel : BaseViewModel
    {
        public ObservableCollection<HospitalResponse> Hospitals { get; }

        public Command LoadHospitalsCommand { get; }

        public Command AddHospitalCommand { get; }

        public HospitalsViewModel()
        {
            Hospitals = new ObservableCollection<HospitalResponse>();
            LoadHospitalsCommand = new Command(async () => await ExecuteLoadItemsCommand());

            AddHospitalCommand = new Command(OnAddItem);
        }

        private async Task ExecuteLoadItemsCommand()
        {
            IsBusy = true;

            try
            {
                var client = Get<IHospitalClient>();

                var hospitals = await client.GetAll();
                Hospitals.Clear();
                foreach (var hospital in hospitals)
                {
                    Hospitals.Add(hospital);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }


        public void OnAppearing()
        {
            IsBusy = true;
        }

        private async void OnAddItem(object obj)
        {
            await Shell.Current.GoToAsync(nameof(NewHospital));
        }
    }
}