﻿using System.Threading.Tasks;
using VaccinationVerifier.Mobile.Clients.Interfaces;
using VaccinationVerifier.Mobile.Views;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace VaccinationVerifier.Mobile.ViewModels
{
    public class LoginViewModel : BaseViewModel
    {
        private readonly IAuthClient _authClient;

        public LoginViewModel()
        {
            _authClient = DependencyService.Get<IAuthClient>();
        }

        private bool _isLoggedIn;

        public bool IsLoggedIn
        {
            get => _isLoggedIn;
            set => SetProperty(ref _isLoggedIn, value);
        }

        public string Error { get; private set; }

        public async Task LoginAsync(string login, string password)
        {
            var request = new Models.LoginRequest
            {
                Login = login,
                Password = password,
            };

            try
            {
                var authenticationResult = await _authClient.LoginAsync(request);
                await SecureStorage.SetAsync("access_token", authenticationResult.Token);
                IsLoggedIn = true;
                await Shell.Current.GoToAsync($"//{nameof(Patients)}");
            }
            catch (System.Exception)
            {
                Error = "Invalid login or password";
            }
        }
    }
}
