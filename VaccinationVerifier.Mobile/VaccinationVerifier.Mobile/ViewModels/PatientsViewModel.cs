﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;
using VaccinationVerifier.Mobile.Clients.Interfaces;
using VaccinationVerifier.Mobile.Models;
using VaccinationVerifier.Mobile.Views;
using Xamarin.Forms;

namespace VaccinationVerifier.Mobile.ViewModels
{
    public class PatientsViewModel : BaseViewModel
    {
        private PatientResponse _selectedItem;

        public ObservableCollection<PatientResponse> Patients { get; }

        public Command LoadPatientsCommand { get; }

        public Command<PatientResponse> PatientTapped { get; }

        public PatientResponse SelectedItem
        {
            get => _selectedItem;
            set
            {
                SetProperty(ref _selectedItem, value);
                OnItemSelected(value);
            }
        }

        public PatientsViewModel()
        {
            Title = "Patient list";
            Patients = new ObservableCollection<PatientResponse>();
            LoadPatientsCommand = new Command(async () => await ExecuteLoadItemsCommand());

            PatientTapped = new Command<PatientResponse>(OnItemSelected);
        }

        private async Task ExecuteLoadItemsCommand()
        {
            IsBusy = true;

            try
            {
                var client = Get<IPatientClient>();

                var patients = await client.GetAll();
                Debug.WriteLine("Patients are fetched");
                Patients.Clear();
                foreach (var patient in patients)
                {
                    Patients.Add(patient);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }

        private static async void OnItemSelected(PatientResponse patientResponse)
        {
            if (patientResponse == null)
                return;

            await Shell.Current.GoToAsync(
                $"{nameof(PatientDetailsPage)}?{nameof(PatientDetailsViewModel.Id)}={patientResponse.Id}");
        }

        public void OnAppearing()
        {
            IsBusy = true;
            SelectedItem = null;
        }
    }
}
