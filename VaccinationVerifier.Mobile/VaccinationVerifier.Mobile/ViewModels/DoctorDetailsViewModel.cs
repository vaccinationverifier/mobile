﻿using System;
using VaccinationVerifier.Mobile.Clients.Interfaces;
using VaccinationVerifier.Mobile.Models;
using Xamarin.Forms;

namespace VaccinationVerifier.Mobile.ViewModels
{
    [QueryProperty(nameof(Id), nameof(Id))]
    public class DoctorDetailsViewModel : BaseViewModel
    {
        private string _id;
        private string _name;
        private string _email;
        private Uri _imageUrl;
        private string _specialty;
        private string _hospital;
        private string _professionalSummary;
        private DoctorVerificationStatus _verification;

        public string Id
        {
            get => _id;
            set
            {
                _id = value;
                LoadItemById();
            }
        }

        public string Name
        {
            get => _name;
            set => SetProperty(ref _name, value);
        }

        public string Email
        {
            get => _email;
            set => SetProperty(ref _email, value);
        }

        public string Specialty
        {
            get => _specialty;
            set => SetProperty(ref _specialty, value);
        }

        public string Hospital
        {
            get => _hospital;
            set => SetProperty(ref _hospital, value);
        }

        public string ProfessionalSummary
        {
            get => _professionalSummary;
            set => SetProperty(ref _professionalSummary, value);
        }

        public DoctorVerificationStatus Verification
        {
            get => _verification;
            set => SetProperty(ref _verification, value);
        }

        public Uri ImageUrl
        {
            get => _imageUrl;
            set => SetProperty(ref _imageUrl, value);
        }

        private async void LoadItemById()
        {
            var client = Get<IDoctorClient>();
            var doctor = await client.GetById(Guid.Parse(_id));

            Name = doctor.Name;
            Email = doctor.Email;
            Specialty = doctor.Specialty;
            ProfessionalSummary = doctor.ProfessionalSummary;
            Verification = doctor.Verification;
            Hospital = doctor.Hospital;
            ImageUrl = doctor.ImageUrl;
        }
    }
}