﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;
using VaccinationVerifier.Mobile.Clients.Interfaces;
using VaccinationVerifier.Mobile.Models;
using VaccinationVerifier.Mobile.Views;
using Xamarin.Forms;

namespace VaccinationVerifier.Mobile.ViewModels
{
    public class VaccinesViewModel : BaseViewModel
    {
        public ObservableCollection<VaccineResponse> Vaccines { get; }

        public Command LoadVaccinesCommand { get; }

        public Command AddVaccineCommand { get; }

        public VaccinesViewModel()
        {
            Vaccines = new ObservableCollection<VaccineResponse>();
            LoadVaccinesCommand = new Command(async () => await ExecuteLoadItemsCommand());

            AddVaccineCommand = new Command(OnAddItem);
        }

        private async Task ExecuteLoadItemsCommand()
        {
            IsBusy = true;

            try
            {
                var client = Get<IVaccineClient>();

                var vaccines = await client.GetAll();
                Vaccines.Clear();
                foreach (var vaccine in vaccines)
                {
                    Vaccines.Add(vaccine);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }

        public void OnAppearing()
        {
            IsBusy = true;
        }

        private async void OnAddItem(object obj)
        {
            await Shell.Current.GoToAsync(nameof(NewVaccine));
        }
    }
}