﻿using System;
using System.Threading.Tasks;
using System.Web;
using VaccinationVerifier.Mobile.Clients.Interfaces;
using VaccinationVerifier.Mobile.Models;
using Xamarin.Forms;

namespace VaccinationVerifier.Mobile.ViewModels
{
    [QueryProperty(nameof(Id), nameof(Id))]
    public class VaccinationDetailsViewModel : BaseViewModel
    {
        private string _id;
        private string _illnessName;
        private string _vaccineName;
        private string _hospitalName;
        private Guid _hospitalId;
        private string _patientName;
        private Guid _doctorId;
        private string _doctorName;
        private VaccinationStatus _vaccinationStatus;
        private DateTime _createdAt;
        private string _vaccinationVideo;

        public string Id
        {
            get => _id;
            set
            {
                _id = value;
                LoadItemById();
            }
        }

        public string IllnessName
        {
            get => _illnessName;
            set => SetProperty(ref _illnessName, value);
        }

        public string VaccineName
        {
            get => _vaccineName;
            set => SetProperty(ref _vaccineName, value);
        }

        public Guid HospitalId
        {
            get => _hospitalId;
            set => SetProperty(ref _hospitalId, value);
        }

        public string HospitalName
        {
            get => _hospitalName;
            set => SetProperty(ref _hospitalName, value);
        }

        public string PatientName
        {
            get => _patientName;
            set => SetProperty(ref _patientName, value);
        }

        public Guid DoctorId
        {
            get => _doctorId;
            set => SetProperty(ref _doctorId, value);
        }

        public string DoctorName
        {
            get => _doctorName;
            set => SetProperty(ref _doctorName, value);
        }

        public VaccinationStatus VaccinationStatus
        {
            get => _vaccinationStatus;
            set => SetProperty(ref _vaccinationStatus, value);
        }

        public DateTime CreatedAt
        {
            get => _createdAt;
            set => SetProperty(ref _createdAt, value);
        }

        public string VaccinationVideo
        {
            get => _vaccinationVideo;
            set => SetProperty(ref _vaccinationVideo, value);
        }

        public bool IsVerified => VaccinationStatus == VaccinationStatus.Validated;

        private async void LoadItemById()
        {
            var client = Get<IVaccinationClient>();
            var vaccination = await client.GetById(Guid.Parse(_id));
            IllnessName = vaccination.IllnessName;
            VaccineName = vaccination.VaccineName;
            HospitalId = vaccination.HospitalId;
            HospitalName = vaccination.HospitalName;
            PatientName = vaccination.PatientName;
            DoctorId = vaccination.DoctorId;
            DoctorName = vaccination.DoctorName;
            VaccinationStatus = vaccination.VaccinationStatus;
            CreatedAt = vaccination.CreatedAt;
            
            VaccinationVideo = HttpUtility.UrlEncode(vaccination.VaccinationVideo.ToString());
        }

        public async Task VerifyAsync()
        {
            var client = Get<IVaccinationClient>();
            await client.VerifyAsync(Guid.Parse(_id));
        }
    }
}