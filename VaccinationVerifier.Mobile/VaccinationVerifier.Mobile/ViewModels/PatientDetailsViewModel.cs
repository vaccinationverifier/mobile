﻿using System;
using System.Globalization;
using VaccinationVerifier.Mobile.Clients.Interfaces;
using VaccinationVerifier.Mobile.Models;
using Xamarin.Forms;

namespace VaccinationVerifier.Mobile.ViewModels
{
    [QueryProperty(nameof(Id), nameof(Id))]
    public class PatientDetailsViewModel : BaseViewModel
    {
        private string _id;
        private string _name;
        private string _email;
        private string _dateOfBirth;
        private Address _address;
        private PatientVerificationStatus _verification;
        private Uri _imageUrl;

        public string Id
        {
            get => _id;
            set
            {
                _id = value;
                LoadItemById();
            }
        }

        public string Name
        {
            get => _name;
            set => SetProperty(ref _name, value);
        }

        public string Email
        {
            get => _email;
            set => SetProperty(ref _email, value);
        }

        public string DateOfBirth
        {
            get => _dateOfBirth;
            set => SetProperty(ref _dateOfBirth, value);
        }

        public Address Address
        {
            get => _address;
            set => SetProperty(ref _address, value);
        }

        public PatientVerificationStatus Verification
        {
            get => _verification;
            set => SetProperty(ref _verification, value);
        }

        public Uri ImageUrl
        {
            get => _imageUrl;
            set => SetProperty(ref _imageUrl, value);
        }

        private async void LoadItemById()
        {
            var client = Get<IPatientClient>();
            var patient = await client.GetById(Guid.Parse(_id));
            Name = patient.Name;
            Email = patient.Email;

            var culture = CultureInfo.CurrentCulture;
            var uiCulture = CultureInfo.CurrentUICulture;
            var formatInfo = CultureInfo.CurrentUICulture.DateTimeFormat;

            DateOfBirth = patient.DateOfBirth.ToString(formatInfo);
            Address = patient.Address;
            Verification = patient.Verification;
            ImageUrl = patient.ImageUrl;
        }
    }
}