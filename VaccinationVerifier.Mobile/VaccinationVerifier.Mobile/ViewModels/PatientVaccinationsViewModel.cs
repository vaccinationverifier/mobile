﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using VaccinationVerifier.Mobile.Clients.Interfaces;
using VaccinationVerifier.Mobile.Models;
using VaccinationVerifier.Mobile.Views;
using Xamarin.Forms;

namespace VaccinationVerifier.Mobile.ViewModels
{
    [QueryProperty(nameof(PatientId), nameof(PatientId))]
    public class PatientVaccinationsViewModel : BaseViewModel
    {
        private VaccinationResponse _selectedItem;
        private string _patientId;

        public string PatientId
        {
            get => _patientId;
            set
            {
                _patientId = value;
                LoadVaccinationsByPatientId();
            }
        }

        public ObservableCollection<VaccinationResponse> Vaccinations { get; }

        public Command<VaccinationResponse> VaccinationTapped { get; }

        public VaccinationResponse SelectedItem
        {
            get => _selectedItem;
            set
            {
                SetProperty(ref _selectedItem, value);
                OnItemSelected(value);
            }
        }

        public PatientVaccinationsViewModel()
        {
            Title = "Patient Vaccinations";
            Vaccinations = new ObservableCollection<VaccinationResponse>();

            VaccinationTapped = new Command<VaccinationResponse>(OnItemSelected);
        }

        private static async void OnItemSelected(VaccinationResponse vaccinationResponse)
        {
            if (vaccinationResponse == null)
                return;

            await Shell.Current.GoToAsync(
                $"{nameof(VaccinationDetailsPage)}?{nameof(VaccinationDetailsViewModel.Id)}={vaccinationResponse.Id}");
        }

        public void OnAppearing()
        {
            IsBusy = true;
            SelectedItem = null;
        }

        private async void LoadVaccinationsByPatientId()
        {
            try
            {
                var client = Get<IVaccinationClient>();

                var vaccinations = await client.GetByPatient(Guid.Parse(_patientId));
                Debug.WriteLine("Vaccinations are fetched");
                Vaccinations.Clear();

                foreach (var vaccination in vaccinations)
                {
                    Vaccinations.Add(vaccination);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
        }
    }
}