﻿using VaccinationVerifier.Mobile.Clients.Interfaces;
using VaccinationVerifier.Mobile.Models;
using Xamarin.Forms;

namespace VaccinationVerifier.Mobile.ViewModels
{
    public class NewVaccineViewModel : BaseViewModel
    {
        private string _illnessName;
        private string _vaccineName;
        private string _type;
        private string _description;
        private string _vendor;
        private string _valence;
        private string _license;
        private string _sideEffects;

        public string IllnessName
        {
            get => _illnessName;
            set => SetProperty(ref _illnessName, value);
        }

        public string VaccineName
        {
            get => _vaccineName;
            set => SetProperty(ref _vaccineName, value);
        }

        public string Type
        {
            get => _type;
            set => SetProperty(ref _type, value);
        }

        public string Description
        {
            get => _description;
            set => SetProperty(ref _description, value);
        }

        public string Vendor
        {
            get => _vendor;
            set => SetProperty(ref _vendor, value);
        }

        public string Valence
        {
            get => _valence;
            set => SetProperty(ref _valence, value);
        }

        public string License
        {
            get => _license;
            set => SetProperty(ref _license, value);
        }

        public string SideEffects
        {
            get => _sideEffects;
            set => SetProperty(ref _sideEffects, value);
        }

        public NewVaccineViewModel()
        {
            SaveCommand = new Command(OnSave, ValidateSave);
            CancelCommand = new Command(OnCancel);
            PropertyChanged +=
                (_, __) => SaveCommand.ChangeCanExecute();
        }
        
        public Command SaveCommand { get; }

        public Command CancelCommand { get; }

        private async void OnSave()
        {
            var request = new CreateVaccineRequest
            {
                VaccineName = VaccineName,
                IllnessName = IllnessName,
                Description = Description,
                License = License,
                SideEffects = SideEffects,
                Type = Type,
                Valence = Valence,
                Vendor = Vendor,
            };

            var client = Get<IVaccineClient>();
            await client.CreateAsync(request);

            // This will pop the current page off the navigation stack
            await Shell.Current.GoToAsync("..");
        }
        
        private async void OnCancel()
        {
            // This will pop the current page off the navigation stack
            await Shell.Current.GoToAsync("..");
        }

        private bool ValidateSave()
        {
            return !string.IsNullOrWhiteSpace(_illnessName)
                   && !string.IsNullOrWhiteSpace(_vaccineName)
                   && !string.IsNullOrWhiteSpace(_type)
                   && !string.IsNullOrWhiteSpace(_description)
                   && !string.IsNullOrWhiteSpace(_vendor)
                   && !string.IsNullOrWhiteSpace(_valence)
                   && !string.IsNullOrWhiteSpace(_license)
                   && !string.IsNullOrWhiteSpace(_sideEffects);
        }
    }
}
