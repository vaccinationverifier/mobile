﻿using VaccinationVerifier.Mobile.Clients.Interfaces;
using VaccinationVerifier.Mobile.Models;
using Xamarin.Forms;

namespace VaccinationVerifier.Mobile.ViewModels
{
    public class NewHospitalViewModel : BaseViewModel
    {
        private string _name;
        private string _street;
        private string _city;
        private string _state;
        private string _country;
        private string _zipCode;

        public NewHospitalViewModel()
        {
            SaveCommand = new Command(OnSave, ValidateSave);
            CancelCommand = new Command(OnCancel);
            PropertyChanged +=
                (_, __) => SaveCommand.ChangeCanExecute();
        }
        
        public string Name
        {
            get => _name;
            set => SetProperty(ref _name, value);
        }

        public string Street
        {
            get => _street;
            set => SetProperty(ref _street, value);
        }

        public string City
        {
            get => _city;
            set => SetProperty(ref _city, value);
        }

        public string State
        {
            get => _state;
            set => SetProperty(ref _state, value);
        }

        public string Country
        {
            get => _country;
            set => SetProperty(ref _country, value);
        }

        public string ZipCode
        {
            get => _zipCode;
            set => SetProperty(ref _zipCode, value);
        }

        public Command SaveCommand { get; }

        public Command CancelCommand { get; }

        private async void OnSave()
        {
            var request = new CreateHospitalRequest
            {
                Name = Name,
                Address = new Address
                {
                    City = City,
                    Country = Country,
                    State = State,
                    Street = Street,
                    ZipCode = ZipCode,
                }
            };

            var client = Get<IHospitalClient>();
            await client.CreateAsync(request);

            // This will pop the current page off the navigation stack
            await Shell.Current.GoToAsync("..");
        }
        
        private async void OnCancel()
        {
            // This will pop the current page off the navigation stack
            await Shell.Current.GoToAsync("..");
        }
        
        private bool ValidateSave()
        {
            return !string.IsNullOrWhiteSpace(_name)
                   && !string.IsNullOrWhiteSpace(_street)
                   && !string.IsNullOrWhiteSpace(_city)
                   && !string.IsNullOrWhiteSpace(_state)
                   && !string.IsNullOrWhiteSpace(_country)
                   && !string.IsNullOrWhiteSpace(_zipCode);
        }
    }
}