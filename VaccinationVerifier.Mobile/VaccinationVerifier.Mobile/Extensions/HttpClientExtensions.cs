﻿using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Xamarin.Essentials;

namespace VaccinationVerifier.Mobile.Extensions
{
    public static class HttpClientExtensions
    {
        public static async Task<T> GetAsync<T>(this HttpClient client, string url)
        {
            await TryAddToken(client);

            var responseMessage = await client.GetAsync(url);
            responseMessage.EnsureSuccessStatusCode();

            return await Deserialize<T>(responseMessage);
        }

        public static async Task<T> PostAsync<T>(this HttpClient client, string url, object request)
        {
            var content = CreateJsonContent(request);
            await TryAddToken(client);
            var responseMessage = await client.PostAsync(url, content);

            return await Deserialize<T>(responseMessage);
        }

        public static async Task PostAsync(this HttpClient client, string url, object request)
        {
            var content = CreateJsonContent(request);
            await TryAddToken(client);
            var responseMessage = await client.PostAsync(url, content);

            responseMessage.EnsureSuccessStatusCode();
        }

        private static StringContent CreateJsonContent(object request)
        {
            var content = JsonConvert.SerializeObject(request);
            var stringContent = new StringContent(content, Encoding.UTF8, "application/json");

            return stringContent;
        }

        private static async Task<T> Deserialize<T>(HttpResponseMessage message)
        {
            var response = await message.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<T>(response);
        }

        private static async Task TryAddToken(HttpClient client)
        {
            var token = await SecureStorage.GetAsync("access_token");

            if (!string.IsNullOrEmpty(token))
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            }
        }
    }
}
