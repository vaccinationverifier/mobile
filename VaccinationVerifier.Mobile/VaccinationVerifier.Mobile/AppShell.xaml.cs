﻿using System;
using System.Collections.Generic;
using VaccinationVerifier.Mobile.ViewModels;
using VaccinationVerifier.Mobile.Views;
using Xamarin.Forms;

namespace VaccinationVerifier.Mobile
{
    public partial class AppShell
    {
        public AppShell()
        {
            InitializeComponent();
            Routing.RegisterRoute(nameof(PatientDetailsPage), typeof(PatientDetailsPage));
            Routing.RegisterRoute(nameof(Patients), typeof(Patients));
            Routing.RegisterRoute(nameof(PatientVaccinationsPage), typeof(PatientVaccinationsPage));
            Routing.RegisterRoute(nameof(VaccinationDetailsPage), typeof(VaccinationDetailsPage));
            Routing.RegisterRoute(nameof(Hospitals), typeof(Hospitals));
            Routing.RegisterRoute(nameof(Vaccines), typeof(Vaccines));
            Routing.RegisterRoute(nameof(NewHospital), typeof(NewHospital));
            Routing.RegisterRoute(nameof(NewVaccine), typeof(NewVaccine));
        }

        private async void OnMenuItemClicked(object sender, EventArgs e)
        {
            await Current.GoToAsync("//LoginPage");
        }
    }
}
