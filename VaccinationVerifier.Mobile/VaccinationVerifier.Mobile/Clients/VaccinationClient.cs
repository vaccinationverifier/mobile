﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using VaccinationVerifier.Mobile.Clients.Interfaces;
using VaccinationVerifier.Mobile.Extensions;
using VaccinationVerifier.Mobile.Models;

namespace VaccinationVerifier.Mobile.Clients
{
    public class VaccinationClient : BaseClient, IVaccinationClient
    {
        public async Task<IEnumerable<VaccinationResponse>> GetByPatient(Guid patientId)
        {
            return await HttpClient.GetAsync<IEnumerable<VaccinationResponse>>($"/patients/{patientId}/vaccinations");
        }

        public async Task<VaccinationResponse> GetById(Guid id)
        {
            return await HttpClient.GetAsync<VaccinationResponse>($"/vaccinations/{id}");
        }

        public async Task VerifyAsync(Guid id)
        {
            await HttpClient.PostAsync($"/vaccinations/{id}/verify", null);
        }
    }
}
