﻿using System.Threading.Tasks;
using VaccinationVerifier.Mobile.Models;

namespace VaccinationVerifier.Mobile.Clients.Interfaces
{
    public interface IAuthClient
    {
        Task<TokenResponse> LoginAsync(LoginRequest request);
    }
}
