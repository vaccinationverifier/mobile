﻿using System;
using System.Threading.Tasks;
using VaccinationVerifier.Mobile.Models;

namespace VaccinationVerifier.Mobile.Clients.Interfaces
{
    public interface IDoctorClient
    {
        Task<DoctorResponse> GetById(Guid id);
    }
}
