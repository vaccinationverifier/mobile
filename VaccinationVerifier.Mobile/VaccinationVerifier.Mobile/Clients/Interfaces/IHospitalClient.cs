﻿using System.Collections.Generic;
using System.Threading.Tasks;
using VaccinationVerifier.Mobile.Models;

namespace VaccinationVerifier.Mobile.Clients.Interfaces
{
    public interface IHospitalClient
    {
        Task CreateAsync(CreateHospitalRequest request);

        Task<IEnumerable<HospitalResponse>> GetAll();
    }
}