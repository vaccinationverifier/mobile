﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using VaccinationVerifier.Mobile.Models;

namespace VaccinationVerifier.Mobile.Clients.Interfaces
{
    public interface IVaccinationClient
    {
        Task<IEnumerable<VaccinationResponse>> GetByPatient(Guid patientId);

        Task<VaccinationResponse> GetById(Guid id);

        Task VerifyAsync(Guid id);
    }
}
