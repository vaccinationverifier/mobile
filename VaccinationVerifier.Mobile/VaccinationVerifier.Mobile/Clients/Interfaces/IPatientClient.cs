﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using VaccinationVerifier.Mobile.Models;

namespace VaccinationVerifier.Mobile.Clients.Interfaces
{
    public interface IPatientClient
    {
        Task<IEnumerable<PatientResponse>> GetAll();

        Task<PatientResponse> GetById(Guid id);
    }
}
