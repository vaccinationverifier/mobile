﻿using System.Collections.Generic;
using System.Threading.Tasks;
using VaccinationVerifier.Mobile.Models;

namespace VaccinationVerifier.Mobile.Clients.Interfaces
{
    public interface IVaccineClient
    {
        Task CreateAsync(CreateVaccineRequest request);

        Task<IEnumerable<VaccineResponse>> GetAll();
    }
}