﻿using System.Collections.Generic;
using System.Threading.Tasks;
using VaccinationVerifier.Mobile.Clients.Interfaces;
using VaccinationVerifier.Mobile.Extensions;
using VaccinationVerifier.Mobile.Models;

namespace VaccinationVerifier.Mobile.Clients
{
    public class VaccineClient : BaseClient, IVaccineClient
    {
        public async Task CreateAsync(CreateVaccineRequest request)
        {
            await HttpClient.PostAsync("vaccines", request);
        }

        public async Task<IEnumerable<VaccineResponse>> GetAll()
        {
            return await HttpClient.GetAsync<IEnumerable<VaccineResponse>>($"/vaccines");
        }
    }
}