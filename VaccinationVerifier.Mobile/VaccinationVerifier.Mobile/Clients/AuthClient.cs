﻿using System.Net.Http;
using System.Threading.Tasks;
using VaccinationVerifier.Mobile.Clients.Interfaces;
using VaccinationVerifier.Mobile.Extensions;
using VaccinationVerifier.Mobile.Models;

namespace VaccinationVerifier.Mobile.Clients
{
    public class AuthClient : BaseClient, IAuthClient
    {
        public async Task<TokenResponse> LoginAsync(LoginRequest request)
        {
            return await HttpClient.PostAsync<TokenResponse>("auth/sign-in", request);
        }
    }
}
