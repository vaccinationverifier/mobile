﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using VaccinationVerifier.Mobile.Clients.Interfaces;
using VaccinationVerifier.Mobile.Constants;
using VaccinationVerifier.Mobile.Models;
using VaccinationVerifier.Mobile.Extensions;

namespace VaccinationVerifier.Mobile.Clients
{
    public class DoctorClient : BaseClient, IDoctorClient
    {
        public async Task<DoctorResponse> GetById(Guid id)
        {
            return await HttpClient.GetAsync<DoctorResponse>($"doctors/{id}");
        }
    }
}
