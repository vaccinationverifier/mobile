﻿using System;
using System.Net.Http;
using VaccinationVerifier.Mobile.Constants;

namespace VaccinationVerifier.Mobile.Clients
{
    public abstract class BaseClient
    {
        protected HttpClient HttpClient { get; }

        public BaseClient()
        {
#if DEBUG
            HttpClientHandler insecureHandler = GetInsecureHandler();
            HttpClient = new HttpClient(insecureHandler);
#else
            HttpClient = new HttpClient();
#endif
            HttpClient.BaseAddress = new Uri($"{ApiConstants.ApiUrl}");
        }

        public HttpClientHandler GetInsecureHandler()
        {
            var handler = new HttpClientHandler
            {
                ServerCertificateCustomValidationCallback = (message, cert, chain, errors) =>
                {
                    if (cert.Issuer.Equals("CN=localhost"))
                        return true;

                    return errors == System.Net.Security.SslPolicyErrors.None;
                }
            };

            return handler;
        }
    }
}
