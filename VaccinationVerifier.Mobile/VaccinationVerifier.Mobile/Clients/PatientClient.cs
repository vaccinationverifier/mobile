﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using VaccinationVerifier.Mobile.Clients.Interfaces;
using VaccinationVerifier.Mobile.Extensions;
using VaccinationVerifier.Mobile.Models;

namespace VaccinationVerifier.Mobile.Clients
{
    public class PatientClient : BaseClient, IPatientClient
    {
        public PatientClient()
        {
        }

        public async Task<IEnumerable<PatientResponse>> GetAll()
        {
            return await HttpClient.GetAsync<IEnumerable<PatientResponse>>("/patients");
        }

        public async Task<PatientResponse> GetById(Guid id)
        {
            return await HttpClient.GetAsync<PatientResponse>($"/patients/{id}");
        }
    }
}
