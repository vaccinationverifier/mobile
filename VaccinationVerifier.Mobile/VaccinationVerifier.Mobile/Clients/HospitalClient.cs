﻿using System.Collections.Generic;
using System.Threading.Tasks;
using VaccinationVerifier.Mobile.Clients.Interfaces;
using VaccinationVerifier.Mobile.Extensions;
using VaccinationVerifier.Mobile.Models;

namespace VaccinationVerifier.Mobile.Clients
{
    public class HospitalClient : BaseClient, IHospitalClient
    {
        public async Task CreateAsync(CreateHospitalRequest request)
        {
            await HttpClient.PostAsync("hospitals", request);
        }

        public async Task<IEnumerable<HospitalResponse>> GetAll()
        {
            return await HttpClient.GetAsync<IEnumerable<HospitalResponse>>($"/hospitals");
        }
    }
}