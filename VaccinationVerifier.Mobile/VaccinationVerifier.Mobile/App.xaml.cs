﻿using VaccinationVerifier.Mobile.Clients;
using VaccinationVerifier.Mobile.Clients.Interfaces;
using VaccinationVerifier.Mobile.Services;
using Xamarin.Forms;

namespace VaccinationVerifier.Mobile
{
    public partial class App : Application
    {

        public App()
        {
            InitializeComponent();

            DependencyService.Register<MockDataStore>();
            DependencyService.Register<IDoctorClient, DoctorClient>();
            DependencyService.Register<IPatientClient, PatientClient>();
            DependencyService.Register<IVaccinationClient, VaccinationClient>();
            DependencyService.Register<IAuthClient, AuthClient>();
            DependencyService.Register<IAuthClient, AuthClient>();
            DependencyService.Register<IVaccineClient, VaccineClient>();
            DependencyService.Register<IHospitalClient, HospitalClient>();

            MainPage = new AppShell();
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
