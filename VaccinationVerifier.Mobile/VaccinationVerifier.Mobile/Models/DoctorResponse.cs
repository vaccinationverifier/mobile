﻿using System;
namespace VaccinationVerifier.Mobile.Models
{
    public class DoctorResponse
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public string Specialty { get; set; }

        public string Hospital { get; set; }

        public int? Experience { get; set; }

        public string ProfessionalSummary { get; set; }

        public DoctorVerificationStatus Verification { get; set; }

        public Uri ImageUrl { get; set; }
    }
}
