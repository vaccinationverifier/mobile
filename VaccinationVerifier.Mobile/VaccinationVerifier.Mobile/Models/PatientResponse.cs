﻿using System;

namespace VaccinationVerifier.Mobile.Models
{
    public class PatientResponse
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public DateTime DateOfBirth { get; set; }

        public Address Address { get; set; }

        public PatientVerificationStatus Verification { get; set; }

        public Uri ImageUrl { get; set; }
    }
}
