﻿using System;

namespace VaccinationVerifier.Mobile.Models
{
    public class VaccinationResponse
    {
        public Guid Id { get; set; }

        public string IllnessName { get; set; }

        public string VaccineName { get; set; }

        public Guid HospitalId { get; set; }

        public string HospitalName { get; set; }

        public string PatientName { get; set; }

        public Guid DoctorId { get; set; }

        public string DoctorName { get; set; }

        public VaccinationStatus VaccinationStatus { get; set; }

        public DateTime CreatedAt { get; set; }

        public Uri VaccinationVideo { get; set; }
    }
}
