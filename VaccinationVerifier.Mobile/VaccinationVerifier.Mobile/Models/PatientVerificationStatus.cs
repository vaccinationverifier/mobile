﻿namespace VaccinationVerifier.Mobile.Models
{
    public enum PatientVerificationStatus
    {
        Pending,
        Approved,
    }
}
