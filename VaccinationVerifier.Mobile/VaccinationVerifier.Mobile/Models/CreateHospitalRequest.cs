﻿namespace VaccinationVerifier.Mobile.Models
{
    public class CreateHospitalRequest
    {
        public string Name { get; set; }

        public Address Address { get; set; }
    }
}