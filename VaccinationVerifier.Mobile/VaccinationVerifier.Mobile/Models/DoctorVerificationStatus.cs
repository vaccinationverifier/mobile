﻿namespace VaccinationVerifier.Mobile.Models
{
    public enum DoctorVerificationStatus
    {
        Pending,
        Approved,
    }
}
