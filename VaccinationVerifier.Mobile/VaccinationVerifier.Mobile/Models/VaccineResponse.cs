﻿using System;

namespace VaccinationVerifier.Mobile.Models
{
    public class VaccineResponse
    {
        public Guid Id { get; set; }

        public string IllnessName { get; set; }

        public string VaccineName { get; set; }

        public string Type { get; set; }

        public string Description { get; set; }

        public string Vendor { get; set; }

        public string Valence { get; set; }

        public string License { get; set; }

        public string SideEffects { get; set; }
    }
}