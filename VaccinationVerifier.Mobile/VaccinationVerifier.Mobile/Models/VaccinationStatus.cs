﻿namespace VaccinationVerifier.Mobile.Models
{
    public enum VaccinationStatus
    {
        Created,
        WaitingForValidation,
        ValidationFailed,
        Validated,
        Confirmed,
    }
}
