﻿using System;

namespace VaccinationVerifier.Mobile.Models
{
    public class HospitalResponse
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public Address Address { get; set; }
    }
}