﻿namespace VaccinationVerifier.Mobile.Models
{
    public class TokenResponse
    {
        public string Token { get; set; }
    }
}
